package me.eddie.l1applications.pjf;

/**
 * Created by Edward on 10/12/2017.
 */
public class FailureOutcome {
    private boolean failed;
    private String cause;

    public FailureOutcome(boolean failed, String cause){
        this.failed = failed;
        this.cause = cause;
    }

    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }
}
