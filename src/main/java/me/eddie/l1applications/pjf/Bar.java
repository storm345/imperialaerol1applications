package me.eddie.l1applications.pjf;

import me.eddie.l1applications.bucklingload.BucklingLoadTestUtil;
import me.eddie.l1applications.materials.MetalProperties;
import me.eddie.l1applications.materials.bars.BarMaterial;
import me.eddie.l1applications.materials.bars.BarMaterials;
import me.eddie.l1applications.materials.gussets.Pins;
import mikera.vectorz.Vector2;

/**
 * Created by Edward on 25/11/2017.
 */
public class Bar {
    private BarMaterial material;
    //Not stored as one vector as calculating vector length is semi costly (sqrt), whereas re-constructing it when needed from components isn't so bad
    private Vector2 directionVec;
    private double length;

    public Bar(BarMaterial material, Vector2 lengthAndDirection) {
        this.material = material;
        setLengthAndDirection(lengthAndDirection);
    }

    public double getMassInGrams(double totalOverhangMM){
        double lengthMM = getLength() * 1000 + totalOverhangMM + Pins.LOADING_PIN.getPinDiameterMM(); //Approximation since doesn't factor in amt removed by each pin
        return lengthMM * getMaterial().getLinearDensity();
    }

    public double getMemberCost(double totalOverhangMM){
        //TODO Ignores length beyond bolts required
        return 0.5 * getMaterial().getCostFactorBeta() * getMassInGrams(totalOverhangMM);
    }

    public boolean willBuckle(double load){
        return getLoadDistanceFromBuckling(load) <= 0;
    }

    public double getLoadDistanceFromBuckling(double load){
        return BucklingLoadTestUtil.getLoadDistanceFromBuckleKN(getMaterial(), MetalProperties.ALUMINIUM_YOUNGS_MODULUS_GPA,
                length*1000.0d, getStressMPa(load), Constraints.SAFETY_TOLERANCE_BUCKLING);
    }

    public double getStressMPa(double load){
        /*System.out.println("Load: "+load);
        System.out.println("Area: "+material.getAreaInMilliMetersSquared()+" ("+ BarMaterials.getMaterialName(material)+")");*/
        return load / material.getAreaInMilliMetersSquared();
    }

    public double getStress(double load){
        return (load / material.getAreaInMilliMetersSquared())*1000000;
    }

    /**
     * Extension is (forcexlength)/(cross-section-area*youngs-modulus)
     * @param load
     * @return
     */
    public double getExtensionForLoad(double load){
        //Use mm^2 and GPa here so that we don't run into double/integer precision issues. Result still converted to metres
        return ((load*getLength()) / (getMaterial().getAreaInMilliMetersSquared() * getMaterial().getYoungsModulusGPA()))/1000;
    }

    public Vector2 getDirectionVector(){
        return this.directionVec.clone();
    }

    public Vector2 getLengthAndDirection() {
        Vector2 v = getDirectionVector().clone();
        v.multiply(getLength());
        return v;
    }

    public void setLengthAndDirection(Vector2 lengthAndDirection) {
        lengthAndDirection = lengthAndDirection.clone();
        this.length = lengthAndDirection.magnitude();
        lengthAndDirection.normalise();
        this.directionVec = lengthAndDirection;
    }

    public double getLength(){
        return this.length;
    }

    public double getAngleFromHorzRadians(){
        return Math.atan(getLengthAndDirection().getY()/getLengthAndDirection().getX());
    }

    public BarMaterial getMaterial() {
        return material;
    }

    public void setMaterial(BarMaterial material) {
        this.material = material;
    }
}
