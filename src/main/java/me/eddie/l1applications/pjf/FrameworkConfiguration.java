package me.eddie.l1applications.pjf;

import me.eddie.l1applications.assertion.Assert;
import me.eddie.l1applications.materials.MetalProperties;
import me.eddie.l1applications.materials.bars.BarMaterial;
import mikera.vectorz.Vector2;

/**
 * Defines a triangular framework configuration for the L1 task.
 */
public class FrameworkConfiguration {
    private int leftHoleNumber = 1; //Which hole on the left side the structure is connected to. 1 is first hole
    private double appliedLoad = 12000; //12 kN
    private double baseConnectionInitialDispAboveE = 0; //The displacement of the bottom attachment point of the frame above E along the line D-E
    private double rightSupportInitialDisp = 0; //The displacement (in positive axis direction) of the right support from the position on the diagram when unloaded
    private Bar bar12 = null;
    private Bar bar13 = null;
    private Bar bar23 = null;
    private GussetConfiguration gussetLeft = null;
    private GussetConfiguration gussetBottom = null;
    private GussetConfiguration gussetRight = null;

    public FrameworkConfiguration(int leftHoleNumber, double appliedLoad, double baseConnectionInitialDispAboveE, double rightSupportInitialDisp,
                                  BarMaterial bar12Material, BarMaterial bar13Material, BarMaterial bar23Material,
                                  GussetConfiguration gussetLeft, GussetConfiguration gussetBottom, GussetConfiguration gussetRight) {
        this.gussetLeft = gussetLeft;
        this.gussetBottom = gussetBottom;
        this.gussetRight = gussetRight;
        this.leftHoleNumber = leftHoleNumber;
        this.appliedLoad = appliedLoad;
        this.baseConnectionInitialDispAboveE = baseConnectionInitialDispAboveE;
        this.rightSupportInitialDisp = rightSupportInitialDisp;

        Vector2 bar12LengthDir = getBottomSupportInitialPos().clone();
        bar12LengthDir.sub(getLeftSupportInitialPos());

        Vector2 bar13LengthDir = getRightSupportInitialPos().clone();
        bar13LengthDir.sub(getLeftSupportInitialPos());

        Vector2 bar23LengthDir = getRightSupportInitialPos().clone();
        bar23LengthDir.sub(getBottomSupportInitialPos());

        //TODO INIT BARS, including length and hole config, etc...
        this.bar12 = new Bar(bar12Material, bar12LengthDir);
        this.bar13 = new Bar(bar13Material, bar13LengthDir);
        this.bar23 = new Bar(bar23Material, bar23LengthDir);
    }

    public GussetConfiguration getGussetLeft() {
        return gussetLeft;
    }

    public void setGussetLeft(GussetConfiguration gussetLeft) {
        this.gussetLeft = gussetLeft;
    }

    public GussetConfiguration getGussetBottom() {
        return gussetBottom;
    }

    public void setGussetBottom(GussetConfiguration gussetBottom) {
        this.gussetBottom = gussetBottom;
    }

    public GussetConfiguration getGussetRight() {
        return gussetRight;
    }

    public void setGussetRight(GussetConfiguration gussetRight) {
        this.gussetRight = gussetRight;
    }

    public double getAppliedLoad() {
        return appliedLoad;
    }

    public void setAppliedLoad(double appliedLoad) {
        this.appliedLoad = appliedLoad;
    }

    public FailureOutcome willFail(){
        double safetyTolerance = Constraints.PREFFERED_SAFETY_TOLERANCE;
        double t12 = getTension12Mag();
        double t13 = getTension13Mag();
        double t23 = getTension23Mag();

        //Tension failures
        if(bar12.getStressMPa(t12) > safetyTolerance*(MetalProperties.ALUMINIUM_PROOF_STRESS/1000000.0)
                || bar23.getStressMPa(t23) > safetyTolerance*(MetalProperties.ALUMINIUM_PROOF_STRESS/1000000.0)
                || bar13.getStressMPa(t13) > safetyTolerance*(MetalProperties.ALUMINIUM_PROOF_STRESS/1000000.0) //Even tho says mag it will be negative if in compression (as is usually)
                ){
            return new FailureOutcome(true, "Member stress exceeds proof stress");
        }

        //Compression failures
        if(
                (t13 < 0 && bar13.willBuckle(t13))
                || (t12 < 0 && bar12.willBuckle(t12))
                        || (t23 < 0 && bar23.willBuckle(t23))
                ){
            return new FailureOutcome(true, "Buckling in compression member(s)");
        }

        //Deflection at load point
        double origLoad = getAppliedLoad();
        setAppliedLoad(Constraints.DESIGNED_LIMIT_LOAD);
        if(Math.abs(getBottomSupportDisplacement().getY()) > Constraints.MAX_VERTICAL_DEFLECTION_DESIGNED_LOAD){
            return new FailureOutcome(true, "Deforms too much with designed limit load");
        }

        //Obstacle clearance
        if(getUndeformedClearanceFromObstacle() < Constraints.OBSTRUCTION_MIN_CLEARANCE
                || getDeformedClearanceFromObstacle() < Constraints.OBSTRUCTION_MIN_CLEARANCE){
            return new FailureOutcome(true, "Hits obstacle");
        }

        //Undo our modification of the config
        setAppliedLoad(origLoad);

        //Pins:

        //Gusset right
        PinConfiguration pinConfig23Right = getPinConfiguration23Right();
        PinFailureOutcome pinFailureOutcomeRight1 = pinConfig23Right.testForFailure();
        if(pinFailureOutcomeRight1.isFailed()){
            return new FailureOutcome(true, "Member 23 right gusset issue: "+pinFailureOutcomeRight1.getCause());
        }
        PinConfiguration pinConfig13Right = getPinConfiguration13Right();
        PinFailureOutcome pinFailureOutcomeRight2 = pinConfig13Right.testForFailure();
        if(pinFailureOutcomeRight2.isFailed()){
            return new FailureOutcome(true, "Member 13 right gusset issue: "+pinFailureOutcomeRight2.getCause());
        }

        //Gusset bottom
        PinConfiguration pinConfig12Bottom = getPinConfiguration12Bottom();
        PinFailureOutcome pinFailureOutcomeBottom1 = pinConfig12Bottom.testForFailure();
        if(pinFailureOutcomeBottom1.isFailed()){
            return new FailureOutcome(true, "Member 12 bottom gusset issue: "+pinFailureOutcomeBottom1.getCause());
        }
        PinConfiguration pinConfig23Bottom = getPinConfiguration23Bottom();
        PinFailureOutcome pinFailureOutcomeBottom2 = pinConfig23Bottom.testForFailure();
        if(pinFailureOutcomeBottom2.isFailed()){
            return new FailureOutcome(true, "Member 23 bottom gusset issue: "+pinFailureOutcomeBottom2.getCause());
        }

        //Gusset left
        //Calculate net opposing force to member 12
        PinConfiguration pinConfig13Left = getPinConfiguration13Left();
        PinFailureOutcome pinFailureOutcomeLeft1 = pinConfig13Left.testForFailure();
        if(pinFailureOutcomeLeft1.isFailed()){
            return new FailureOutcome(true, "Member 13 left gusset issue: "+pinFailureOutcomeLeft1.getCause());
        }
        PinConfiguration pinConfig12Left = getPinConfiguration12Left();
        PinFailureOutcome pinFailureOutcomeLeft2 = pinConfig12Left.testForFailure();
        if(pinFailureOutcomeLeft2.isFailed()){
            return new FailureOutcome(true, "Member 12 left gusset issue: "+pinFailureOutcomeLeft2.getCause());
        }

        return new FailureOutcome(false, "");
    }

    public double getGussetBottomAreaMMSq(){
        Vector2 member1Dir = bar12.getDirectionVector().clone();
        member1Dir.multiply(-1);
        Vector2 member2Dir = bar23.getDirectionVector().clone();
        return getGussetAreaMMSq(member1Dir, member2Dir,
                getPinConfiguration12Bottom(),
                getPinConfiguration23Bottom());
    }

    public double getGussetLeftAreaMMSq(){
        Vector2 member1Dir = bar13.getDirectionVector().clone();
        Vector2 member2Dir = bar12.getDirectionVector().clone();
        return getGussetAreaMMSq(member1Dir, member2Dir,
                getPinConfiguration13Left(),
                getPinConfiguration12Left());
    }

    public double getGussetRightAreaMMSq(){
        Vector2 member1Dir = bar23.getDirectionVector().clone();
        member1Dir.multiply(-1);
        Vector2 member2Dir = bar13.getDirectionVector().clone();
        member2Dir.multiply(-1);
        return getGussetAreaMMSq(member1Dir, member2Dir,
                getPinConfiguration23Right(),
                getPinConfiguration13Right());
    }

    public double getGussetAreaMMSq(Vector2 member1Dir, Vector2 member2Dir,
                                    PinConfiguration member1Pins, PinConfiguration member2Pins){
        double cosAngle = member1Dir.clone().dotProduct(member2Dir);
        double sinAngle = Math.sin(Math.acos(cosAngle));
        return 0.5 * member1Pins.getTotalGussetLength() * member2Pins.getTotalGussetLength() * sinAngle;
    }

    public PinConfiguration getPinConfiguration23Bottom(){
        Vector2 t12V = bar12.getDirectionVector();
        t12V.multiply(getTension12Mag());
        Vector2 t21V = t12V;
        t21V.multiply(-1);
        Vector2 loadVector = new Vector2(0, -getAppliedLoad());
        Vector2 dirOpposedTo23At2 = bar23.getDirectionVector();
        dirOpposedTo23At2.normalise();
        dirOpposedTo23At2.multiply(-1);
        double extForceOpposingBar23At2 = loadVector.clone().dotProduct(dirOpposedTo23At2);
        double intForceOpposingBar23At2 = t21V.clone().dotProduct(dirOpposedTo23At2);
        double opposingForceTo23At2 = Math.max(extForceOpposingBar23At2, extForceOpposingBar23At2+intForceOpposingBar23At2);
        return gussetBottom.getPinConfigurationMember2(bar23, getTension23Mag(), opposingForceTo23At2);
    }

    public PinConfiguration getPinConfiguration12Bottom(){
        Vector2 t23V = bar23.getDirectionVector();
        t23V.multiply(getTension23Mag());
        Vector2 loadVector = new Vector2(0, -getAppliedLoad());
        Vector2 dirOpposedTo12At2 = bar12.getDirectionVector();
        dirOpposedTo12At2.normalise();
        double extForceOpposingBar12At2 = loadVector.clone().dotProduct(dirOpposedTo12At2);
        double intForceOpposingBar12At2 = t23V.clone().dotProduct(dirOpposedTo12At2);
        double opposingForceTo12At2 = Math.max(extForceOpposingBar12At2, extForceOpposingBar12At2+intForceOpposingBar12At2);
        return gussetBottom.getPinConfigurationMember1(bar12, getTension12Mag(), opposingForceTo12At2);
    }

    public PinConfiguration getPinConfiguration13Right(){
        Vector2 t23V = bar23.getDirectionVector();
        t23V.multiply(getTension23Mag());
        Vector2 t32V = t23V;
        t32V.multiply(-1);
        Vector2 dirOpposedTo13At3 = bar13.getDirectionVector();
        dirOpposedTo13At3.normalise();
        double extForceOpposingBar13At3 = getRightSupportReaction().clone().dotProduct(dirOpposedTo13At3);
        double intForceOpposingBar13At3 = t32V.clone().dotProduct(dirOpposedTo13At3);
        double opposingForceTo13At3 = Math.max(extForceOpposingBar13At3, extForceOpposingBar13At3+intForceOpposingBar13At3);
        return gussetRight.getPinConfigurationMember2(bar13, getTension13Mag(), opposingForceTo13At3);
    }

    public PinConfiguration getPinConfiguration23Right(){
        Vector2 t13V = bar13.getDirectionVector();
        t13V.multiply(getTension13Mag());
        Vector2 t31V = t13V;
        t31V.multiply(-1);
        Vector2 dirOpposedTo23At3 = bar23.getDirectionVector();
        dirOpposedTo23At3.normalise();
        double extForceOpposingBar23At3 = getRightSupportReaction().dotProduct(dirOpposedTo23At3);
        double intForceOpposingBar23At3 = t31V.dotProduct(dirOpposedTo23At3);
        double opposingForceTo23At3 = Math.max(extForceOpposingBar23At3, extForceOpposingBar23At3+intForceOpposingBar23At3);
        return gussetRight.getPinConfigurationMember1(bar23, getTension23Mag(), opposingForceTo23At3);
    }

    public PinConfiguration getPinConfiguration12Left(){
        Vector2 t13V = bar13.getDirectionVector();
        t13V.multiply(getTension13Mag());
        Vector2 t31V = t13V;
        t31V.multiply(-1);
        Vector2 dirOpposedTo12At1 = bar12.getDirectionVector();
        dirOpposedTo12At1.normalise();
        dirOpposedTo12At1.multiply(-1);
        double extForceOpposingBar12At1 = getLeftSupportReaction().dotProduct(dirOpposedTo12At1);
        double intForceOpposingBar12At1 = t13V.dotProduct(dirOpposedTo12At1);
        double opposingForceTo12At1 = Math.max(extForceOpposingBar12At1, extForceOpposingBar12At1+intForceOpposingBar12At1);
        return gussetLeft.getPinConfigurationMember2(bar12, getTension12Mag(), opposingForceTo12At1);
    }

    public PinConfiguration getPinConfiguration13Left(){
        Vector2 dirOpposedTo13At1 = bar13.getDirectionVector();
        dirOpposedTo13At1.normalise();
        dirOpposedTo13At1.multiply(-1);
        Vector2 t12V = bar12.getDirectionVector();
        t12V.multiply(getTension12Mag());
        double extForceOpposingBar13At1 = getLeftSupportReaction().dotProduct(dirOpposedTo13At1);
        double intForceOpposingBar13At1 = t12V.dotProduct(dirOpposedTo13At1);
        double opposingForceTo13At1 = Math.max(extForceOpposingBar13At1, extForceOpposingBar13At1+intForceOpposingBar13At1);
        PinConfiguration pinConfig13Left = gussetLeft.getPinConfigurationMember1(bar13, getTension13Mag(), opposingForceTo13At1);
        return pinConfig13Left;
    }

    public double getBar12TotalOverhang(){
        return getPinConfiguration12Bottom().getMemberOverhangAtEnd()
                + getPinConfiguration12Left().getMemberOverhangAtEnd();
    }

    public double getBar13TotalOverhang(){
        return getPinConfiguration13Left().getMemberOverhangAtEnd()
                + getPinConfiguration13Right().getMemberOverhangAtEnd();
    }

    public double getBar23TotalOverhang(){
        return getPinConfiguration23Bottom().getMemberOverhangAtEnd()
                +getPinConfiguration23Right().getMemberOverhangAtEnd();
    }

    public double getTotalMassGrams(){
        double mass = 0;

        //Mass of bar members
        mass += bar12.getMassInGrams(getBar12TotalOverhang())
                + bar13.getMassInGrams(getBar13TotalOverhang())
                + bar23.getMassInGrams(getBar23TotalOverhang());

        mass += gussetLeft.getGussetMaterial().getAreaDensity()*getGussetLeftAreaMMSq()
                + gussetRight.getGussetMaterial().getAreaDensity()*getGussetRightAreaMMSq()
                + gussetBottom.getGussetMaterial().getAreaDensity()*getGussetBottomAreaMMSq();
        return mass;
    }

    public double getTotalCost(){
        double cost = 0;

        //Cost of bar members
        cost += bar12.getMemberCost(getBar12TotalOverhang()) +
                bar13.getMemberCost(getBar13TotalOverhang()) +
                bar23.getMemberCost(getBar23TotalOverhang());

        //Cost of gussets
        double gussetMass = gussetLeft.getGussetMaterial().getAreaDensity()*getGussetLeftAreaMMSq()
                + gussetRight.getGussetMaterial().getAreaDensity()*getGussetRightAreaMMSq()
                + gussetBottom.getGussetMaterial().getAreaDensity()*getGussetBottomAreaMMSq();
        cost += gussetMass * 0.9;

        PinConfiguration[] pinConfigurations = new PinConfiguration[]{
                getPinConfiguration23Right(),getPinConfiguration13Right(),
                getPinConfiguration12Left(),getPinConfiguration13Left(),
                getPinConfiguration12Bottom(),getPinConfiguration23Bottom()
        };
        double totalHoles = 0;
        for(PinConfiguration config:pinConfigurations){
            cost += config.getNumOfPins()*config.getPinsUsed().getCostFactor();
            totalHoles += config.getNumOfPins();
        }

        //Labour costs
        cost += totalHoles*0.5;
        cost += 6*10; //6 gussets at 10 units per gusset
        return cost;
    }

    public double getMeritFunction(){
        return getAppliedLoad() / ((getTotalMassGrams()/1000.0d) * getTotalCost());
    }

    public double getUndeformedClearanceFromObstacle(){
        double bar13YMag = getLeftSupportInitialPos().getY() - getRightSupportInitialPos().getY();
        double bar13XMag = getRightSupportInitialPos().getX() - getLeftSupportInitialPos().getX();
        double xObstacle = Constraints.OBSTRUCTION_CENTER_LOC.clone().getX()-getLeftSupportInitialPos().getX();
        double yOfBarDirectlyAboveObstacleRelToC = (bar13YMag*(1-(xObstacle/bar13XMag))); //Extrapolate along straight line
        double changeInYBetweenBarAndObstacle = Math.abs(yOfBarDirectlyAboveObstacleRelToC + getRightSupportInitialPos().getY()-Constraints.OBSTRUCTION_CENTER_LOC.getY());
        double distanceBarToCenter = changeInYBetweenBarAndObstacle * Math.cos(Math.atan(bar13YMag/bar13XMag)); //Trig
        double distanceBarToEdge = distanceBarToCenter - Constraints.OBSTRUCTION_RADIUS;
        double clearance = distanceBarToEdge-(bar13.getMaterial().getSizeInXYPlane()/2);
        return clearance;
    }

    public double getDeformedClearanceFromObstacle(){
        Vector2 leftSupport = getLeftSupportInitialPos().clone();
        leftSupport.add(getLeftSupportDisplacement());
        Vector2 rightSupport = getRightSupportInitialPos().clone();
        rightSupport.add(getRightSupportDisplacement());

        double bar13YMag = leftSupport.getY() - rightSupport.getY();
        double bar13XMag = rightSupport.getX() - leftSupport.getX();
        double xObstacle = Constraints.OBSTRUCTION_CENTER_LOC.clone().getX()-leftSupport.getX();
        double yOfBarDirectlyAboveObstacleRelToC = (bar13YMag*(1-(xObstacle/bar13XMag))); //Extrapolate along straight line
        double changeInYBetweenBarAndObstacle = Math.abs(yOfBarDirectlyAboveObstacleRelToC + rightSupport.getY()-Constraints.OBSTRUCTION_CENTER_LOC.getY());
        double distanceBarToCenter = changeInYBetweenBarAndObstacle * Math.cos(Math.atan(bar13YMag/bar13XMag)); //Trig
        double distanceBarToEdge = distanceBarToCenter - Constraints.OBSTRUCTION_RADIUS;
        double clearance = distanceBarToEdge-(bar13.getMaterial().getSizeInXYPlane()/2);
        return clearance;
    }

    public void validateDisplacementsWork(){ //TODO Call this method at some point
        double t = 0.001; //Tolerance of 1 mm
        Vector2 r2 = getBottomSupportDisplacement();
        Vector2 r3 = getRightSupportDisplacement();
        Assert.assertEquals(getExt13(), r3.getX()*Math.cos(getInitialAlphaRadians()), t);
        Assert.assertEquals(getExt12(), r2.getX()*Math.cos(getInitialGammaRadians()) - r2.getY()*Math.sin(getInitialGammaRadians()),t);
        Assert.assertEquals(getExt23(), r3.getX()*Math.cos(getInitialBetaRadians()) - r2.getX()*Math.cos(getInitialBetaRadians()) - r2.getY()*Math.sin(getInitialBetaRadians()), t);
    }

    public double getExt12(){
        return bar12.getExtensionForLoad(getTension12Mag());
    }

    public double getExt13(){
        return bar13.getExtensionForLoad(getTension13Mag());
    }

    public double getExt23(){
        return bar23.getExtensionForLoad(getTension23Mag());
    }

    public Vector2 getLeftSupportDisplacement(){
        return new Vector2(0,0);
    }

    public Vector2 getRightSupportDisplacement(){
        return new Vector2(getExt13() / Math.cos(getInitialAlphaRadians()), 0);
    }

    /**
     * From hand calculations
     */
    public Vector2 getBottomSupportDisplacement(){
        double x3 = getRightSupportDisplacement().getX();
        double y = (x3*Math.cos(getInitialBetaRadians()) - getExt23() - ((getExt12()*Math.cos(getInitialBetaRadians()))/Math.cos(getInitialGammaRadians())))
                / (Math.tan(getInitialGammaRadians())*Math.cos(getInitialBetaRadians()) + Math.sin(getInitialBetaRadians()));
        double x = (getExt12() + y*Math.sin(getInitialGammaRadians())) / Math.cos(getInitialGammaRadians());
        return new Vector2(x,y);
    }

    public double getBar13LoadDistanceFromBuckling(){
        return bar13.getLoadDistanceFromBuckling(getTension13Mag());
    }

    public boolean willBar13Buckle(){
        return getBar13LoadDistanceFromBuckling() <= 0;
    }

    public double getStressMPa13(){
        return bar13.getStressMPa(getTension13Mag());
    }

    public double getStressMPa12(){
        return bar12.getStressMPa(getTension12Mag());
    }

    public double getStressMPa23(){
        return bar23.getStressMPa(getTension23Mag());
    }

    /**
     * Equations derived from hand calculations
     */
    public double getTension12Mag(){
        double t12 = getLeftSupportReaction().getY() / (Math.sin(getInitialGammaRadians()) - Math.cos(getInitialGammaRadians())*Math.tan(getInitialAlphaRadians()));
        return t12;
    }

    /**
     * Equations derived from hand calculations
     */
    public double getTension13Mag(){
        double t13 = (-getTension12Mag() * Math.cos(getInitialGammaRadians()) / Math.cos(getInitialAlphaRadians()));
        return t13;
    }

    /**
     * Equations derived from hand calculations
     */
    public double getTension23Mag(){
        double t23 = getAppliedLoad() / (Math.cos(getInitialBetaRadians()) * Math.tan(getInitialGammaRadians()) + Math.sin(getInitialBetaRadians()));
        return t23;
    }

    /**
     * Equations derived from hand calculations
     */
    public Vector2 getLeftSupportReaction(){
        double v1 = getAppliedLoad()* ((bar23.getLength()*Math.cos(getInitialBetaRadians())) / (bar13.getLength()*Math.cos(getInitialAlphaRadians())));
        double h1 = 0;
        return new Vector2(h1, v1);
    }

    /**
     * Equations derived from hand calculations
     */
    public Vector2 getRightSupportReaction(){
        double v3 = getAppliedLoad()-getLeftSupportReaction().getY();
        double h3 = 0;
        return new Vector2(h3, v3);
    }

    /**
     * Alpha is angle between bar 13 and horizontal
     */
    public double getInitialAlphaRadians(){
        return -bar13.getAngleFromHorzRadians();
    }

    /**
     * Beta is angle between bar 23 and horizontal
     */
    public double getInitialBetaRadians(){
        return bar23.getAngleFromHorzRadians();
    }

    /**
     * Gamma is angle between bar 12 and horizontal
     */
    public double getInitialGammaRadians(){
        return -bar12.getAngleFromHorzRadians();
    }

    public Vector2 getBottomSupportInitialPos(){
        Vector2 v = Constraints.POSITION_E_LOC.clone();
        v.add(new Vector2(0, baseConnectionInitialDispAboveE));
        return v;
    }

    public Vector2 getLeftSupportInitialPos(){
        Vector2 v = Constraints.TOP_LEFT_HOLE_POSITION.clone();
        v.sub(new Vector2(0, (getLeftHoleNumber()-1)*Constraints.LEFT_HOLE_SPACING));
        return v;
    }

    public Vector2 getRightSupportInitialPos(){
        Vector2 v = Constraints.RIGHT_SUPPORT_DEFAULT_LOC.clone();
        v.add(new Vector2(getRightSupportInitialDisp(), 0));
        return v;
    }

    public Bar getBar12() {
        return bar12;
    }

    public void setBar12(Bar bar12) {
        this.bar12 = bar12;
    }

    public Bar getBar13() {
        return bar13;
    }

    public void setBar13(Bar bar13) {
        this.bar13 = bar13;
    }

    public Bar getBar23() {
        return bar23;
    }

    public void setBar23(Bar bar23) {
        this.bar23 = bar23;
    }

    public int getLeftHoleNumber() {
        return leftHoleNumber;
    }

    public void setLeftHoleNumber(int leftHoleNumber) {
        this.leftHoleNumber = leftHoleNumber;
    }

    public double getBaseConnectionInitialDispAboveE() {
        return baseConnectionInitialDispAboveE;
    }

    public void setBaseConnectionInitialDispAboveE(double baseConnectionInitialDispAboveE) {
        this.baseConnectionInitialDispAboveE = baseConnectionInitialDispAboveE;
    }

    public double getRightSupportInitialDisp() {
        return rightSupportInitialDisp;
    }

    public void setRightSupportInitialDisp(double rightSupportInitialDisp) {
        this.rightSupportInitialDisp = rightSupportInitialDisp;
    }
}
