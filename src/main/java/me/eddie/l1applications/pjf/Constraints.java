package me.eddie.l1applications.pjf;

import mikera.vectorz.Vector2;

/**
 * All units everywhere are SI units (Eg. metres, Newtons, etc...)
 * Coordinates are defined from origin marked on paper diagram (200mm below center of hole 10 in the left frame)
 *
 * Node 1 is left support node
 * Node 2 is bottom support node
 * Node 3 is right support node
 */
public class Constraints {
    public static final double MAX_VERTICAL_DEFLECTION_DESIGNED_LOAD = 0.005; //5 mm in metres
    public static final double DESIGNED_LIMIT_LOAD = 8000; //8 Kilonewtons
    public static final double ULTIMATE_LOAD = 12000; //12 Kilonewtons
    public static final double LOADING_PIN_DIAMETER = 0.008; //8 mm loading pins
    public static final Vector2 OBSTRUCTION_CENTER_LOC = new Vector2(0.35, 0.3);
    public static final double OBSTRUCTION_RADIUS = 0.051;
    public static final double OBSTRUCTION_MIN_CLEARANCE = 0.005; //5mm clearance of hole at all times
    public static final double PREFFERED_SAFETY_TOLERANCE = 0.88;//0.8; //80% of theoretical limit
    public static final double BAR12_NET_TENSION_SAFETY_TOLERANCE = 0.8; //80% of theoretical limit
    public static final Vector2 RIGHT_SUPPORT_DEFAULT_LOC = new Vector2(0.8, 0.2);
    public static final double RIGHT_SUPPORT_MAX_ABS_DISPLACEMENT = 0.009; //Been told can move about 10mm either side of marked position, but using 9mm for extra precaution
    public static final Vector2 POSITION_E_LOC = new Vector2(0.35, 0);
    public static final Vector2 TOP_LEFT_HOLE_POSITION = new Vector2(0, 0.65);
    public static final double LEFT_HOLE_SPACING = 0.05;
    public static final double SAFETY_TOLERANCE_BUCKLING = 0.8;
    public static final double MAX_PIN_GROUP_LENGTH_MM = 200;
    public static final double MAX_END_OF_LOADING_PIN_OVERHANG = 30;
}
