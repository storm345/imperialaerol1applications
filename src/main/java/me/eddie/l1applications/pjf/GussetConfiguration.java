package me.eddie.l1applications.pjf;

import me.eddie.l1applications.materials.gussets.GussetMaterial;
import me.eddie.l1applications.materials.gussets.GussetMaterials;
import me.eddie.l1applications.materials.gussets.Pin;
import me.eddie.l1applications.materials.gussets.Pins;

/**
 * Created by Edward on 10/12/2017.
 */
public class GussetConfiguration {
    private GussetMaterial gussetMaterial;
    private int numOfPinsMember1 = 4;
    private int numOfPinsMember2 = 4;
    private Pin pinTypeMember1 = Pins.BOLT_3;
    private Pin pinTypeMember2 = Pins.BOLT_3;
    private boolean loadingPinThroughMember1 = false;
    private boolean loadingPinThroughMember2 = false;

    public GussetConfiguration(GussetMaterial gussetMaterial, int numOfPinsMember1, int numOfPinsMember2, Pin pinTypeMember1, Pin pinTypeMember2, boolean loadingPinThroughMember1, boolean loadingPinThroughMember2) {
        this.gussetMaterial = gussetMaterial;
        this.numOfPinsMember1 = numOfPinsMember1;
        this.numOfPinsMember2 = numOfPinsMember2;
        this.pinTypeMember1 = pinTypeMember1;
        this.pinTypeMember2 = pinTypeMember2;
        this.loadingPinThroughMember1 = loadingPinThroughMember1;
        this.loadingPinThroughMember2 = loadingPinThroughMember2;
    }

    public void printOut(){
        System.out.println("Gusset material: "+ GussetMaterials.getMaterialName(getGussetMaterial()));
        System.out.println("Is loading pin through left member (facing in): "+loadingPinThroughMember1);
        System.out.println("Is loading pin through right member (facing in): "+loadingPinThroughMember2);
    }

    public PinConfiguration getPinConfigurationMember1(Bar member, double memberTension, double opposedLoad){
        PinConfiguration pinConfiguration = loadingPinThroughMember1 ?
                PinConfiguration.newMemberToLoadingPinConfig(member, this.gussetMaterial, memberTension, opposedLoad, numOfPinsMember1, pinTypeMember1, Pins.LOADING_PIN)
                : PinConfiguration.newMemberToGussetConfig(member, this.gussetMaterial, memberTension, numOfPinsMember1, pinTypeMember1);
        return pinConfiguration;
    }

    public PinConfiguration getPinConfigurationMember2(Bar member, double memberTension, double opposedLoad){
        PinConfiguration pinConfiguration = loadingPinThroughMember2 ?
                PinConfiguration.newMemberToLoadingPinConfig(member, this.gussetMaterial, memberTension, opposedLoad, numOfPinsMember2, pinTypeMember2, Pins.LOADING_PIN)
                : PinConfiguration.newMemberToGussetConfig(member, this.gussetMaterial, memberTension, numOfPinsMember2, pinTypeMember2);
        return pinConfiguration;
    }

    public GussetMaterial getGussetMaterial() {
        return gussetMaterial;
    }

    public void setGussetMaterial(GussetMaterial gussetMaterial) {
        this.gussetMaterial = gussetMaterial;
    }

    public int getNumOfPinsMember1() {
        return numOfPinsMember1;
    }

    public void setNumOfPinsMember1(int numOfPinsMember1) {
        this.numOfPinsMember1 = numOfPinsMember1;
    }

    public int getNumOfPinsMember2() {
        return numOfPinsMember2;
    }

    public void setNumOfPinsMember2(int numOfPinsMember2) {
        this.numOfPinsMember2 = numOfPinsMember2;
    }

    public Pin getPinTypeMember1() {
        return pinTypeMember1;
    }

    public void setPinTypeMember1(Pin pinTypeMember1) {
        this.pinTypeMember1 = pinTypeMember1;
    }

    public Pin getPinTypeMember2() {
        return pinTypeMember2;
    }

    public void setPinTypeMember2(Pin pinTypeMember2) {
        this.pinTypeMember2 = pinTypeMember2;
    }

    public boolean isLoadingPinThroughMember1() {
        return loadingPinThroughMember1;
    }

    public void setLoadingPinThroughMember1(boolean loadingPinThroughMember1) {
        this.loadingPinThroughMember1 = loadingPinThroughMember1;
    }

    public boolean isLoadingPinThroughMember2() {
        return loadingPinThroughMember2;
    }

    public void setLoadingPinThroughMember2(boolean loadingPinThroughMember2) {
        this.loadingPinThroughMember2 = loadingPinThroughMember2;
    }
}
