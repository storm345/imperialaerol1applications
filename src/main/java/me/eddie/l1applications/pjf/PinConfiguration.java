package me.eddie.l1applications.pjf;

import me.eddie.l1applications.materials.MetalProperties;
import me.eddie.l1applications.materials.bars.BarMaterial;
import me.eddie.l1applications.materials.bars.BarMaterials;
import me.eddie.l1applications.materials.gussets.GussetMaterial;
import me.eddie.l1applications.materials.gussets.GussetMaterials;
import me.eddie.l1applications.materials.gussets.Pin;
import me.eddie.l1applications.materials.gussets.Pins;
import mikera.vectorz.Vector2;

/**
 * Created by Edward on 10/12/2017.
 */
public class PinConfiguration {
    public static void main(String[] args){
        /*PinConfiguration config = new PinConfiguration(BarMaterials.FLAT_127_32, GussetMaterials.SHEET_1_5MM,
                3700, 0, 0, 1850, 4,
                Pins.BOLT_4);*/
        /*PinConfiguration config = PinConfiguration.newMemberToGussetConfig(BarMaterials.FLAT_127_32, GussetMaterials.SHEET_1_5MM, 3700,
                4, Pins.BOLT_4);*/
        Bar bar = new Bar(BarMaterials.FLAT_254_32, new Vector2(1,1));
        PinConfiguration config = PinConfiguration.newMemberToLoadingPinConfig(bar, GussetMaterials.SHEET_1_5MM, 6800,
                12000, 4, Pins.BOLT_4, Pins.LOADING_PIN);
        /*PinConfiguration config = new PinConfiguration(BarMaterials.FLAT_254_32, GussetMaterials.SHEET_1_5MM,
                6800, 6240, 0, 2880, 4,
                Pins.BOLT_4);*/
        config.printOut();
    }

    public void printOut(){
        PinConfiguration config = this;
        System.out.println("Pin type used: "+Pins.getPinName(getPinsUsed())+" ("+getPinsUsed().getPinDiameterMM()+")");
        System.out.println("Num of pins: "+getNumOfPins());
        //System.out.println("Load per pin: "+config.getLoadPerPin()+"N");
        System.out.println("Member overhang at end (mm): "+config.getMemberOverhangAtEnd()+" (+ve in direction towards end of member)");
        System.out.println("Gusset overhang at end (mm): "+config.getGussetOverhangAtEnd()+" (+ve in direction towards end of member)");
        System.out.println("Pin spacing required (mm): "+config.getRequiredPinSpacing());
        System.out.println("Pin shear: "+(getLoadPerPin()*100.0d/(getCriticalPinShearLoad()/Constraints.PREFFERED_SAFETY_TOLERANCE))+"% of max");
        System.out.println("Member net tension: "+getMemberNetTensionPropOfMax()*100+"% of max");
        System.out.println("Member bearing failure: "+getLoadPerPin()*100.0d/(getCriticalMemberBearingFailureLoad()/Constraints.PREFFERED_SAFETY_TOLERANCE)+"% of max");
        System.out.println("Gusset net tension: "+getGussetNetTensionPropOfMax()*100+"% of max");
        System.out.println("Gusset bearing failure: "+getLoadPerPin()*50.0d/((getGussetCriticalBearingFailureLoad())/Constraints.PREFFERED_SAFETY_TOLERANCE)+"% of max");
        if(loadingPin != null){
            System.out.println("Loading Pin Member net tension: "+Math.abs(memberRightLoad)*100/(getCriticalMemberLoadingPinNetTension()/ Constraints.PREFFERED_SAFETY_TOLERANCE)+"% of max");
            System.out.println("Loading Pin Member bearing failure: "+Math.abs(memberRightLoad)*100/(getCriticalMemberLoadingPinBearingLoad()/ Constraints.PREFFERED_SAFETY_TOLERANCE)+"% of max");
            System.out.println("Loading Pin Gusset net tension: "+Math.abs(gussetRightLoadPerGusset)*100/(getCriticalGussetLoadingPinNetTension()/Constraints.PREFFERED_SAFETY_TOLERANCE)+"% of max");
            System.out.println("Loading Pin Gusset bearing failure: "+Math.abs(gussetRightLoadPerGusset)*100/(getCriticalGussetLoadingPinBearingLoad()/Constraints.PREFFERED_SAFETY_TOLERANCE)+"% of max");
        }
        /*System.out.println("Crit. pin shear load: "+(config.getCriticalPinShearLoad()/Constraints.PREFFERED_SAFETY_TOLERANCE) +"N");
        System.out.println("Fails by pin shear: "+config.failsByPinShear());
        System.out.println("Crit. member net tension: "+(config.getCriticalMemberNetTension()/Constraints.PREFFERED_SAFETY_TOLERANCE) +"N");
        System.out.println("Member fails by net tension: "+config.memberFailsByNetTension());
        System.out.println("Req pin spacing: "+config.getRequiredPinSpacing()+"mm");
        System.out.println("Crit. member shear out load: "+(config.getCriticalMemberShearOutLoad(25)/Constraints.PREFFERED_SAFETY_TOLERANCE) +"N");
        System.out.println("Member fails by shear out: "+config.memberFailsByShearOut(25));
        System.out.println("Crit. member bearing load: "+(config.getCriticalMemberBearingFailureLoad()/Constraints.PREFFERED_SAFETY_TOLERANCE) +"N");
        System.out.println("Member fails by bearing failure: "+config.memberFailsByBearingFailure());
        System.out.println("Crit. gusset net tension: "+(config.getCriticalGussetNetTensionLoad()/Constraints.PREFFERED_SAFETY_TOLERANCE) +"N");
        System.out.println("Gusset fails by net tension: "+config.gussetFailsByNetTension());
        System.out.println("Crit. gusset shear out load: "+(config.getGussetCriticalShearOutLoad(25)/Constraints.PREFFERED_SAFETY_TOLERANCE) +"N");
        System.out.println("Gusset fails by shear out: "+config.gussetFailsByShearOut(25));
        System.out.println("Crit. gusset bearing load: "+(config.getGussetCriticalBearingFailureLoad()/Constraints.PREFFERED_SAFETY_TOLERANCE) +"N");
        System.out.println("Gusset fails by bearing failure: "+config.gussetFailsByBearingFailure());

        System.out.println("Loading pin:");
        System.out.println("Crit. member loading pin net tension: "+(config.getCriticalMemberLoadingPinNetTension()/Constraints.PREFFERED_SAFETY_TOLERANCE)+"N");
        System.out.println("Member fails by loading pin net tension: "+config.memberFailsByLoadingPinNetTension());
        System.out.println("Crit. member loading pin shear out load: "+(config.getCriticalMemberLoadingPinShearOutLoad(6)/Constraints.PREFFERED_SAFETY_TOLERANCE)+"N");
        System.out.println("Member fails by loading pin shear out: "+config.memberFailsByLoadingPinShearOut(6));
        System.out.println("Required member overhang to prevent shear out: "+config.getRequiredMemberOverhangNearLoadingPin());
        System.out.println("Crit. member loading pin bearing load: "+(config.getCriticalMemberLoadingPinBearingLoad()/Constraints.PREFFERED_SAFETY_TOLERANCE)+"N");
        System.out.println("Member fails by loading pin bearing load: "+config.memberFailsByLoadingPinBearingLoad());
        System.out.println("Crit. gusset loading pin net tension: "+(config.getCriticalGussetLoadingPinNetTension()/Constraints.PREFFERED_SAFETY_TOLERANCE)+"N");
        System.out.println("Gusset fails by loading pin net tension: "+config.gussetFailsByLoadingPinNetTension());
        System.out.println("Crit. gusset loading pin shear out load: "+(config.getCriticalGussetLoadingPinShearOutLoad(6)/Constraints.PREFFERED_SAFETY_TOLERANCE)+"N");
        System.out.println("Gusset fails by loading pin shear out load: "+config.gussetFailsByLoadingPinShearOut(6));
        System.out.println("Required gusset overhang to prevent shear out: "+config.getRequiredGussetOverhangNearLoadingPin());
        System.out.println("Crit. gusset loading pin bearing load: "+(config.getCriticalGussetLoadingPinBearingLoad()/Constraints.PREFFERED_SAFETY_TOLERANCE)+"N");
        System.out.println("Gusset fails by loading pin bearing load: "+config.gussetFailsByLoadingPinBearingLoad());*/
    }

    private BarMaterial barMaterial;
    private Vector2 barLengthAndDir;
    private GussetMaterial gussetMaterial;
    private double memberLeftLoad = 3700;
    private double memberRightLoad = 0;
    private double gussetLeftLoadPerGusset = 0;
    private double gussetRightLoadPerGusset = 1850;
    private int numOfPins = 4;
    private Pin pinsUsed;

    private Pin loadingPin = null; //Is null if no loading pin considered

    public static PinConfiguration newMemberToGussetConfig(Bar bar, GussetMaterial gussetMaterial,
                                                           double memberTension, int numOfPins, Pin pinsUsed){
        return new PinConfiguration(bar.getLengthAndDirection(), bar.getMaterial(),
                gussetMaterial,
                memberTension,0,
                0,memberTension/2.0,
                numOfPins,pinsUsed,null);
    }

    public static PinConfiguration newMemberToLoadingPinConfig(Bar bar, GussetMaterial gussetMaterial,
                                                               double memberTension, double loadOnLoadingPin, int numOfPinsExclLoadingPin,
                                                               Pin pinsUsed, Pin loadingPin){
        BarMaterial barMaterial = bar.getMaterial();
        double barThicknessMM = barMaterial.getThickness() * 1000;
        double gussetThicknessMM = gussetMaterial.getThicknessMM();
        double totalThicknessMM = barThicknessMM + 2*gussetThicknessMM;
        double rightMemberLoad = loadOnLoadingPin * (barThicknessMM/totalThicknessMM);
        double rightGussetLoad = loadOnLoadingPin * (gussetThicknessMM/totalThicknessMM);
        return new PinConfiguration(bar.getLengthAndDirection(), barMaterial, gussetMaterial, memberTension, rightMemberLoad, 0, rightGussetLoad, numOfPinsExclLoadingPin,
                pinsUsed, loadingPin);
    }


    public PinConfiguration(Vector2 barLengthAndDir, BarMaterial barMaterial, GussetMaterial gussetMaterial, double memberLeftLoad, double memberRightLoad, double gussetLeftLoadPerGusset, double gussetRightLoadPerGusset, int numOfPins, Pin pinsUsed,
                            Pin loadingPin) {
        this.barLengthAndDir = barLengthAndDir;
        this.barMaterial = barMaterial;
        this.gussetMaterial = gussetMaterial;
        this.memberLeftLoad = memberLeftLoad;
        this.memberRightLoad = memberRightLoad;
        this.gussetLeftLoadPerGusset = gussetLeftLoadPerGusset;
        this.gussetRightLoadPerGusset = gussetRightLoadPerGusset;
        this.numOfPins = numOfPins;
        this.pinsUsed = pinsUsed;
        this.loadingPin = loadingPin;
    }

    public PinFailureOutcome testForFailure(){
        if(failsByPinShear()){
            return new PinFailureOutcome(true, "Pin shear");
        }
        if(memberFailsByNetTension()){
            return new PinFailureOutcome(true, "Member net tension failure");
        }
        if(memberFailsByBearingFailure()){
            return new PinFailureOutcome(true, "Member failed by bearing failure");
        }
        if(getRequiredPinSpacing() > (Constraints.MAX_PIN_GROUP_LENGTH_MM/numOfPins)){
            return new PinFailureOutcome(true, "Pin spacing required to avoid shear out is ridiculous");
        }
        if(gussetFailsByNetTension()){
            return new PinFailureOutcome(true, "Gusset fails by net tension");
        }
        if(gussetFailsByBearingFailure()){
            return new PinFailureOutcome(true, "Gusset fails by bearing failure");
        }
        if(loadingPin != null){
            if(memberFailsByLoadingPinNetTension()){
                return new PinFailureOutcome(true, "Member fails by net tension at loading pin sandwich");
            }
            if(getRequiredMemberOverhangNearLoadingPin() > Constraints.MAX_END_OF_LOADING_PIN_OVERHANG){
                return new PinFailureOutcome(true, "Member would need to overhang loading pin too much");
            }
            if(getRequiredGussetOverhangNearLoadingPin() > Constraints.MAX_END_OF_LOADING_PIN_OVERHANG){
                return new PinFailureOutcome(true, "Gusset would need to overhang loading pin too much");
            }
            if(memberFailsByLoadingPinBearingLoad()){
                return new PinFailureOutcome(true, "Member fails by bearing load at loading pin sandwich");
            }
            if(gussetFailsByLoadingPinNetTension()){
                return new PinFailureOutcome(true, "Gusset fails by net tension at loading pin sandwich");
            }
            if(gussetFailsByLoadingPinBearingLoad()){
                return new PinFailureOutcome(true, "Gusset fails by bearing load at loading pin sandwich");
            }
        }

        return new PinFailureOutcome(false, "");
    }

    public BarMaterial getBarMaterial() {
        return barMaterial;
    }

    public void setBarMaterial(BarMaterial barMaterial) {
        this.barMaterial = barMaterial;
    }

    public Vector2 getBarLengthAndDir() {
        return barLengthAndDir;
    }

    public void setBarLengthAndDir(Vector2 barLengthAndDir) {
        this.barLengthAndDir = barLengthAndDir;
    }

    public GussetMaterial getGussetMaterial() {
        return gussetMaterial;
    }

    public void setGussetMaterial(GussetMaterial gussetMaterial) {
        this.gussetMaterial = gussetMaterial;
    }

    public double getMemberLeftLoad() {
        return memberLeftLoad;
    }

    public void setMemberLeftLoad(double memberLeftLoad) {
        this.memberLeftLoad = memberLeftLoad;
    }

    public double getMemberRightLoad() {
        return memberRightLoad;
    }

    public void setMemberRightLoad(double memberRightLoad) {
        this.memberRightLoad = memberRightLoad;
    }

    public double getGussetLeftLoadPerGusset() {
        return gussetLeftLoadPerGusset;
    }

    public void setGussetLeftLoadPerGusset(double gussetLeftLoadPerGusset) {
        this.gussetLeftLoadPerGusset = gussetLeftLoadPerGusset;
    }

    public double getGussetRightLoadPerGusset() {
        return gussetRightLoadPerGusset;
    }

    public void setGussetRightLoadPerGusset(double gussetRightLoadPerGusset) {
        this.gussetRightLoadPerGusset = gussetRightLoadPerGusset;
    }

    public int getNumOfPins() {
        return numOfPins;
    }

    public void setNumOfPins(int numOfPins) {
        this.numOfPins = numOfPins;
    }

    public Pin getPinsUsed() {
        return pinsUsed;
    }

    public void setPinsUsed(Pin pinsUsed) {
        this.pinsUsed = pinsUsed;
    }

    public Pin getLoadingPin() {
        return loadingPin;
    }

    public void setLoadingPin(Pin loadingPin) {
        this.loadingPin = loadingPin;
    }

    public double getLoadChangeMember(){
        return Math.abs(memberLeftLoad - memberRightLoad);
    }

    public double getLoadPerPin(){
        return getLoadChangeMember() / ((double)numOfPins);
    }

    public double getCriticalPinShearLoad(){
        double maxPinLoad = pinsUsed.getDoubleShearFailureLoadKN()*1000;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * maxPinLoad;
    }

    public boolean failsByPinShear(){
        return getLoadPerPin() > getCriticalPinShearLoad();
    }

    public double getCriticalMemberNetTension(){
        double barWidthMM = barMaterial.getSize()*1000;
        double thicknessMM = barMaterial.getThickness()*1000;
        double proofStressMPa = MetalProperties.ALUMINIUM_PROOF_STRESS / 1000000.0;
        double maxTensionN = (barWidthMM-pinsUsed.getPinDiameterMM()) * thicknessMM * proofStressMPa;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * maxTensionN;
    }

    public double getMemberNetTensionPropOfMax(){
        return Math.abs(Math.max(Math.abs(memberLeftLoad), Math.abs(memberLeftLoad)) / (getCriticalMemberNetTension()/ Constraints.PREFFERED_SAFETY_TOLERANCE));
    }

    public boolean memberFailsByNetTension(){
        double proportionOfMax = getMemberNetTensionPropOfMax();
        //Allow higher proportion of max for 3mm pins in bar12
        if(proportionOfMax > Constraints.PREFFERED_SAFETY_TOLERANCE){
            if(barLengthAndDir.getY() < 0 && pinsUsed.getPinDiameterMM() <= 3){ //Hack-ish way of checking
                return proportionOfMax > Constraints.BAR12_NET_TENSION_SAFETY_TOLERANCE;
            }
        }
        return Math.max(Math.abs(memberLeftLoad), Math.abs(memberLeftLoad)) > getCriticalMemberNetTension();
    }

    public double getRequiredPinSpacingToSatisfyShearOutMember(){
        double pinLoad = getLoadPerPin();
        double thicknessMM = barMaterial.getThickness()*1000;
        double ultShearStressMPa = MetalProperties.ALUMINIUM_ULT_SHEAR_STRESS / 1000000.0;
        return (pinLoad / (2*thicknessMM*ultShearStressMPa))/Constraints.PREFFERED_SAFETY_TOLERANCE;
    }

    public double getCriticalMemberBearingFailureLoad(){
        double thicknessMM = barMaterial.getThickness()*1000;
        double ultBearingStressMPa = MetalProperties.ALUMINIUM_ULT_BEARING_STRESS / 1000000.0;
        return Constraints.PREFFERED_SAFETY_TOLERANCE*(thicknessMM*pinsUsed.getPinDiameterMM()*ultBearingStressMPa);
    }

    public boolean memberFailsByBearingFailure(){
        return getLoadPerPin() > getCriticalMemberBearingFailureLoad();
    }

    public double getCriticalGussetNetTensionLoad(){
        double gussetWidthMM = barMaterial.getSize()*1000; //TODO Gusset width = member width
        double thicknessMM = gussetMaterial.getThicknessMM();
        double proofStressMPa = MetalProperties.ALUMINIUM_PROOF_STRESS / 1000000.0;
        double maxTensionN = (gussetWidthMM-pinsUsed.getPinDiameterMM()) * thicknessMM * proofStressMPa;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * maxTensionN;
    }

    public double getGussetNetTensionPropOfMax(){
        return Math.max(Math.abs(gussetLeftLoadPerGusset), Math.abs(gussetRightLoadPerGusset)) / (getCriticalGussetNetTensionLoad() / Constraints.PREFFERED_SAFETY_TOLERANCE);
    }

    public boolean gussetFailsByNetTension(){
        //TODO May say it will fail since once of our net tension is about 0.9 of max value
        return Math.max(Math.abs(gussetLeftLoadPerGusset), Math.abs(gussetRightLoadPerGusset)) > getCriticalGussetNetTensionLoad();
    }

    public double getCriticalMemberShearOutLoad(double pinSpacingMM){
        double thicknessMM = barMaterial.getThickness() * 1000;
        double ultShearStressMPa = MetalProperties.ALUMINIUM_ULT_SHEAR_STRESS / 1000000.0;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * (2*pinSpacingMM*thicknessMM*ultShearStressMPa);
    }

    public boolean memberFailsByShearOut(double pinSpacingMM){
        double load = getLoadPerPin();
        return load > getCriticalMemberShearOutLoad(pinSpacingMM);
    }

    public double getGussetCriticalShearOutLoad(double pinSpacingMM){
        double thicknessMM = gussetMaterial.getThicknessMM();
        double ultShearStressMPa = MetalProperties.ALUMINIUM_ULT_SHEAR_STRESS / 1000000.0;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * (2*pinSpacingMM*thicknessMM*ultShearStressMPa);
    }

    public boolean gussetFailsByShearOut(double pinSpacingMM){
        double load = getLoadPerPin()/2.0; //Halved since 2 gussets
        return load > getGussetCriticalShearOutLoad(pinSpacingMM);
    }

    public double getRequiredPinSpacingToSatisfyShearOutGusset(){
        double load = getLoadPerPin() / 2.0; //Halved since 2 gussets
        double thicknessMM = gussetMaterial.getThicknessMM();
        double ultShearStressMPa = MetalProperties.ALUMINIUM_ULT_SHEAR_STRESS / 1000000.0;
        return (load / (2*thicknessMM*ultShearStressMPa))/Constraints.PREFFERED_SAFETY_TOLERANCE;
    }

    public double getRequiredPinSpacing(){
        return Math.max(getRequiredPinSpacingToSatisfyShearOutGusset(),
                getRequiredPinSpacingToSatisfyShearOutMember());
    }

    public double getGussetCriticalBearingFailureLoad(){
        double thicknessMM = gussetMaterial.getThicknessMM();
        double ultBearingStressMPa = MetalProperties.ALUMINIUM_ULT_BEARING_STRESS / 1000000.0;
        return Constraints.PREFFERED_SAFETY_TOLERANCE*(thicknessMM*pinsUsed.getPinDiameterMM()*ultBearingStressMPa);
    }

    public boolean gussetFailsByBearingFailure(){
        double load = getLoadPerPin() / 2.0; //Halved since 2 gussets
        return load > getGussetCriticalBearingFailureLoad();
    }

    public double getCriticalMemberLoadingPinNetTension(){
        if(loadingPin == null){
            return Double.MAX_VALUE;
        }
        double barWidthMM = barMaterial.getSize()*1000;
        double thicknessMM = barMaterial.getThickness()*1000;
        double proofStressMPa = MetalProperties.ALUMINIUM_PROOF_STRESS / 1000000.0;
        double maxTensionN = (barWidthMM-loadingPin.getPinDiameterMM()) * thicknessMM * proofStressMPa;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * maxTensionN;
    }

    public boolean memberFailsByLoadingPinNetTension(){
        return memberRightLoad > getCriticalMemberLoadingPinNetTension();
    }

    public double getCriticalMemberLoadingPinShearOutLoad(double overhangMM){
        if(loadingPin == null){
            return Double.MAX_VALUE;
        }
        double thicknessMM = barMaterial.getThickness() * 1000;
        double ultShearStressMPa = MetalProperties.ALUMINIUM_ULT_SHEAR_STRESS / 1000000.0;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * (2*overhangMM*thicknessMM*ultShearStressMPa);
    }

    public boolean memberFailsByLoadingPinShearOut(double overhangMM){
        return memberRightLoad > getCriticalMemberLoadingPinShearOutLoad(overhangMM);
    }

    public double getRequiredMemberOverhangNearLoadingPin(){
        if(loadingPin == null){
            return 0;
        }
        double pinLoad = memberRightLoad;
        double thicknessMM = barMaterial.getThickness()*1000;
        double ultShearStressMPa = MetalProperties.ALUMINIUM_ULT_SHEAR_STRESS / 1000000.0;
        return (pinLoad / (2*thicknessMM*ultShearStressMPa))/Constraints.PREFFERED_SAFETY_TOLERANCE;
    }

    public double getCriticalMemberLoadingPinBearingLoad(){
        if(loadingPin == null){
            return Double.MAX_VALUE;
        }
        double thicknessMM = barMaterial.getThickness() * 1000;
        double bearingStressMPa = MetalProperties.ALUMINIUM_ULT_BEARING_STRESS/ 1000000.0;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * (thicknessMM * loadingPin.getPinDiameterMM() * bearingStressMPa);
    }

    public boolean memberFailsByLoadingPinBearingLoad(){
        return memberRightLoad > getCriticalMemberLoadingPinBearingLoad();
    }

    public double getCriticalGussetLoadingPinNetTension(){
        if(loadingPin == null){
            return Double.MAX_VALUE;
        }
        double gussetwidthMM = barMaterial.getSize()*1000; //TODO Gusset width modelled as being member width
        double thicknessMM = gussetMaterial.getThicknessMM();
        double proofStressMPa = MetalProperties.ALUMINIUM_PROOF_STRESS / 1000000.0;
        double maxTensionN = (gussetwidthMM-loadingPin.getPinDiameterMM()) * thicknessMM * proofStressMPa;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * maxTensionN;
    }

    public boolean gussetFailsByLoadingPinNetTension(){
        return gussetRightLoadPerGusset > getCriticalGussetLoadingPinNetTension();
    }

    public double getCriticalGussetLoadingPinShearOutLoad(double overhangMM){
        if(loadingPin == null){
            return Double.MAX_VALUE;
        }
        double thicknessMM = gussetMaterial.getThicknessMM();
        double ultShearStressMPa = MetalProperties.ALUMINIUM_ULT_SHEAR_STRESS / 1000000.0;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * (2*overhangMM*thicknessMM*ultShearStressMPa);
    }

    public boolean gussetFailsByLoadingPinShearOut(double overhangMM){
        return gussetRightLoadPerGusset > getCriticalGussetLoadingPinShearOutLoad(overhangMM);
    }

    public double getRequiredGussetOverhangNearLoadingPin(){
        if(loadingPin == null){
            return 0;
        }
        double pinLoad = gussetRightLoadPerGusset;
        double thicknessMM = gussetMaterial.getThicknessMM();
        double ultShearStressMPa = MetalProperties.ALUMINIUM_ULT_SHEAR_STRESS / 1000000.0;
        return (pinLoad / (2*thicknessMM*ultShearStressMPa))/Constraints.PREFFERED_SAFETY_TOLERANCE;
    }

    public double getCriticalGussetLoadingPinBearingLoad(){
        double thicknessMM = gussetMaterial.getThicknessMM();
        double bearingStressMPa = MetalProperties.ALUMINIUM_ULT_BEARING_STRESS / 1000000.0;
        return Constraints.PREFFERED_SAFETY_TOLERANCE * (thicknessMM*loadingPin.getPinDiameterMM() *bearingStressMPa);
    }

    public boolean gussetFailsByLoadingPinBearingLoad(){
        return gussetRightLoadPerGusset > getCriticalGussetLoadingPinBearingLoad();
    }

    public double getTotalGussetLength(){
        return getRequiredGussetOverhangNearLoadingPin()+(getNumOfPins()+1)*getRequiredPinSpacing()
                + pinsUsed.getPinDiameterMM()*getNumOfPins()
                + (loadingPin==null?0:loadingPin.getPinDiameterMM());
    }

    public double getGussetOverhangAtEnd(){
        if(this.loadingPin != null){
            return getRequiredGussetOverhangNearLoadingPin();
        }
        return getRequiredPinSpacing();
    }

    public double getMemberOverhangAtEnd(){
        if(this.loadingPin != null){
            return getRequiredMemberOverhangNearLoadingPin();
        }
        return getRequiredPinSpacing();
    }
}
