package me.eddie.l1applications.bucklingload;

import me.eddie.l1applications.materials.MetalProperties;
import me.eddie.l1applications.materials.bars.BarMaterial;

/**
 * Can be used to mathematically determine if a beam will buckle or not for a given compressive load.
 * Works using Euler's critical buckling load equation
 * Moments of inertia derived by hand and double checked via values against https://skyciv.com/free-moment-of-inertia-calculator/
 * Unit tested against known values calculated both via hand and via matlab script where this was prototyped and graphed (for comparison against given graph)
 * Incorrectly handles envelope above material shear stress, intentionally as shouldn't be required by modelling as almost impossible to need it.
 */
public class BucklingLoadTestUtil {
    /**
     * Will bar buckle within given tolerance?
     * tolerance of 0.8 will give safe results hopefully
     */
    public static double getLoadDistanceFromBuckleKN(BarMaterial barMaterial, double youngsModulusGPa,
                                     double lengthmm,
                                     double stressMPa, double tolerance){
        stressMPa = Math.abs(stressMPa);
        double loadKN = (stressMPa * barMaterial.getAreaInMilliMetersSquared()) / 1000.0;
        double I = calculateMinimumSecondMomentOfAreaInMMSq(barMaterial);
        double criticalLoadKN = getEulerCriticalLoad(youngsModulusGPa, I, lengthmm, 1);
        if(criticalLoadKN*1000 > MetalProperties.ALUMINIUM_ULT_TENS_COMP_STRESS){
            criticalLoadKN = MetalProperties.ALUMINIUM_ULT_TENS_COMP_STRESS / 1000;
        }
        if(criticalLoadKN*1000 > MetalProperties.ALUMINIUM_PROOF_STRESS){
            criticalLoadKN = MetalProperties.ALUMINIUM_PROOF_STRESS / 1000;
        }
        if(criticalLoadKN*1000 > MetalProperties.ALUMINIUM_ULT_SHEAR_STRESS){
            //Don't model above shear stress - Can be modelled with a straight line as demonstrated on provided graph but shouldn't be required for the problem we are tackling
            criticalLoadKN = MetalProperties.ALUMINIUM_ULT_SHEAR_STRESS / 1000;
        }
        return (criticalLoadKN*tolerance)-(loadKN); //Scale up by tolerance factor (Eg. divide by 0.8 for 80%) and then check if still less than calculated critical load
    }

    /**
     * Will bar buckle within given tolerance?
     * tolerance of 0.8 will give safe results hopefully
     */
    public static boolean willBuckle(BarMaterial barMaterial, double youngsModulusGPa,
                                     double lengthmm,
                                     double stressMPa, double tolerance){
        return getLoadDistanceFromBuckleKN(barMaterial, youngsModulusGPa, lengthmm, stressMPa, tolerance) <= 0;
    }

    public static double getEulerCriticalLoad(double youngsModulus, double secondMomentOfArea, double length, double k){
        return (Math.pow(Math.PI,2) * youngsModulus * secondMomentOfArea) / Math.pow(k*length,2);
    }

    public static double calculateMinimumSecondMomentOfAreaInMMSq(BarMaterial barMaterial){
        double sizemm = barMaterial.getSize()*1000;
        double thicknessmm = barMaterial.getThickness()*1000;

        double b,t;
        switch (barMaterial.getBarShape()){
            case FLAT:
                double h = Math.min(sizemm, thicknessmm);
                b = h == sizemm ? thicknessmm : sizemm; //The other of sizemm and thicknessmm
                return (Math.pow(h,3)*b)/12;
            case ANGLE:
                //TODO Maybe work this out
                break;
            case CHANNEL:
                //TODO Work out the eqns and put this in
                break;
            case SQ_TUBE:
                //TODO These I values are correct (checked against tool online). However gives higher predicted buckling failure points than given graph
                double outerDiamater = sizemm;
                double innerDiameter = sizemm-(2*thicknessmm);
                double I = (Math.pow(outerDiamater,3)*outerDiamater)/12 - (Math.pow(innerDiameter,3)*innerDiameter)/12;
                return I;
            case T_SHAPE:
                b = sizemm;
                t = thicknessmm;
                double Iy = ((Math.pow(b,3)*t)/6) + Math.pow((b/2),2)*t*b*2 + (Math.pow((t*2),3)*(b-t))/12;
                //Finding Ix
                //First find x centroid
                double totalA = t*2*b+2*t*(b-t);
                double momentOfArea = (t/2)* t*2*b + (((b-t)/2)+t)* 2*t*(b-t);
                double xCentroid = (momentOfArea) / totalA;
                //Calculate 2nd moment of area about centroidal x axis
                double IxCross = (Math.pow(t,3)*b*2)/12;
                double crossTransfer = Math.pow((xCentroid-(t/2)),2) * 2*b*t;
                double IxLeg = ((Math.pow((b-t),3)*(2*t))/12);
                double legTransfer = 2*t*(b-t) * Math.pow((xCentroid - (0.5*(b-t)+t)),2);
                double Ix = IxCross + crossTransfer + IxLeg+legTransfer;
                return Math.min(Ix, Iy);
        }

        return 0;
    }


}
