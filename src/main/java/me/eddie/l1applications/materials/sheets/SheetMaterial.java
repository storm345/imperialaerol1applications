package me.eddie.l1applications.materials.sheets;

/**
 * Created by Edward on 25/11/2017.
 */
public class SheetMaterial {
    private double size; //Not quite sure what this dimension is, but it's been converted from 'swg' to metres
    private double thickness;
    private double linearDensity;

    public SheetMaterial(double size, double thickness, double linearDensity) {
        this.size = size;
        this.thickness = thickness;
        this.linearDensity = linearDensity;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getThickness() {
        return thickness;
    }

    public void setThickness(double thickness) {
        this.thickness = thickness;
    }

    public double getLinearDensity() {
        return linearDensity;
    }

    public void setLinearDensity(double linearDensity) {
        this.linearDensity = linearDensity;
    }
}
