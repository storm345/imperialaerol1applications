package me.eddie.l1applications.materials.sheets;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edward on 25/11/2017.
 */
public class SheetMaterials {

    public static SheetMaterial SHEET_20swg = new SheetMaterial(0.000914, 0.0010, 0.0025);
    public static SheetMaterial SHEET_18swg = new SheetMaterial(0.001219, 0.0012, 0.0033);
    public static SheetMaterial SHEET_16swg = new SheetMaterial(0.001626, 0.0015, 0.0044);


    public static List<SheetMaterial> getValues(){
        Class<SheetMaterials> claz = SheetMaterials.class;
        List<SheetMaterial> res = new ArrayList<SheetMaterial>();
        Field[] fields = claz.getDeclaredFields();
        for(Field f:fields){
            f.setAccessible(true);
            try {
                res.add((SheetMaterial) f.get(null));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return res;
    }
}
