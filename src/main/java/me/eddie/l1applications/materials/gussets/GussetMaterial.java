package me.eddie.l1applications.materials.gussets;

/**
 * Created by Edward on 10/12/2017.
 */
public class GussetMaterial {
    private double thicknessMM;
    private double areaDensity; //g per mm^2

    public GussetMaterial(double thicknessMM, double areaDensityGPerMMSq){
        this.thicknessMM = thicknessMM;
        this.areaDensity = areaDensityGPerMMSq;
    }

    public double getThicknessMM() {
        return thicknessMM;
    }

    public void setThicknessMM(double thicknessMM) {
        this.thicknessMM = thicknessMM;
    }

    public double getAreaDensity() {
        return areaDensity;
    }

    public void setAreaDensity(double areaDensity) {
        this.areaDensity = areaDensity;
    }
}
