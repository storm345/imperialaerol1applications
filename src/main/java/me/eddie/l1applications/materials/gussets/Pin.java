package me.eddie.l1applications.materials.gussets;

/**
 * Created by Edward on 10/12/2017.
 */
public class Pin {
    private double pinDiameterMM;
    private double doubleShearFailureLoadKN;
    private double costFactor;

    public Pin(double pinDiameterMM, double doubleShearFailureLoadKN, double costFactor) {
        this.pinDiameterMM = pinDiameterMM;
        this.doubleShearFailureLoadKN = doubleShearFailureLoadKN;
        this.costFactor = costFactor;
    }

    public void setPinDiameterMM(double pinDiameterMM) {
        this.pinDiameterMM = pinDiameterMM;
    }

    public double getCostFactor() {
        return costFactor;
    }

    public void setCostFactor(double costFactor) {
        this.costFactor = costFactor;
    }

    public double getPinDiameterMM() {
        return pinDiameterMM;
    }

    public void setPinDiameter(double pinDiameterMM) {
        this.pinDiameterMM = pinDiameterMM;
    }

    public double getDoubleShearFailureLoadKN() {
        return doubleShearFailureLoadKN;
    }

    public void setDoubleShearFailureLoadKN(double doubleShearFailureLoadKN) {
        this.doubleShearFailureLoadKN = doubleShearFailureLoadKN;
    }
}
