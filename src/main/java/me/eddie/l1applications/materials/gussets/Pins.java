package me.eddie.l1applications.materials.gussets;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edward on 10/12/2017.
 */
public class Pins {

    //public static Pin RIVET_3_2 = new Pin(3.2, 1.6, 0.4);
    //public static Pin RIVET_4_0 = new Pin(4.0, 2.66, 0.8);
    //public static Pin RIVET_4_8 = new Pin(4.8, 4.04, 1.2);
    public static Pin BOLT_6 = new Pin(6, 11.2, 2.0);
    public static Pin BOLT_5 = new Pin(5, 7.8, 1.6);
    public static Pin BOLT_4 = new Pin(4, 4.8, 1.2);
    public static Pin BOLT_3 = new Pin(3, 2.8, 1);

    public static Pin LOADING_PIN = new Pin(8, Double.MAX_VALUE, 0);

    public static String getPinName(Pin bm){
        Class<Pins> claz = Pins.class;
        Field[] fields = claz.getDeclaredFields();
        for(Field f:fields){
            f.setAccessible(true);
            try {
                if(bm.equals(f.get(null))){
                    return f.getName();
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public static List<Pin> getJoiningPins(){
        Class<Pins> claz = Pins.class;
        List<Pin> res = new ArrayList<Pin>();
        Field[] fields = claz.getDeclaredFields();
        for(Field f:fields){
            f.setAccessible(true);
            try {
                res.add((Pin) f.get(null));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        res.remove(LOADING_PIN);
        return res;
    }
}
