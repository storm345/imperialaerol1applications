package me.eddie.l1applications.materials.gussets;

import me.eddie.l1applications.materials.bars.BarMaterial;
import me.eddie.l1applications.materials.bars.BarShape;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edward on 25/11/2017.
 */
public class GussetMaterials {

    public static GussetMaterial SHEET_1MM = new GussetMaterial(1, 0.0025);
    public static GussetMaterial SHEET_1_2MM = new GussetMaterial(1.2, 0.0033);
    public static GussetMaterial SHEET_1_5MM = new GussetMaterial(1.5, 0.0044);

    public static String getMaterialName(GussetMaterial bm){
        Class<GussetMaterials> claz = GussetMaterials.class;
        Field[] fields = claz.getDeclaredFields();
        for(Field f:fields){
            f.setAccessible(true);
            try {
                if(bm.equals(f.get(null))){
                    return f.getName();
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public static List<GussetMaterial> getValues(){
        Class<GussetMaterials> claz = GussetMaterials.class;
        List<GussetMaterial> res = new ArrayList<GussetMaterial>();
        Field[] fields = claz.getDeclaredFields();
        for(Field f:fields){
            f.setAccessible(true);
            try {
                res.add((GussetMaterial) f.get(null));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return res;
    }
}
