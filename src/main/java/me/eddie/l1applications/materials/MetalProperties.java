package me.eddie.l1applications.materials;

/**
 * Created by Edward on 25/11/2017.
 */
public class MetalProperties {
    public static double ALUMINIUM_PROOF_STRESS = 240000000; //240 Newtons per mm squared 0.2% proof stress
    public static double ALUMINIUM_ULT_TENS_COMP_STRESS = 280000000;
    public static double ALUMINIUM_ULT_BEARING_STRESS = 450000000;
    public static double ALUMINIUM_ULT_SHEAR_STRESS = 180000000;
    public static double ALUMINIUM_DENSITY = 2710;
    public static long ALUMINIUM_YOUNGS_MODULUS_GPA = 72;
    public static long ALUMINIUM_YOUNGS_MODULUS = 72000000000L; //Needs to be a long as very large
}
