package me.eddie.l1applications.materials.bars;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edward on 25/11/2017.
 */
public class BarMaterials {
    public static BarMaterial FLAT_127_16 = new BarMaterial(1, BarShape.FLAT, 0.0127, 0.0016, 0.0127, 20.3, 0.055);
    public static BarMaterial FLAT_127_32 = new BarMaterial(1, BarShape.FLAT, 0.0127, 0.0032, 0.0127, 40.6, 0.110);
    public static BarMaterial FLAT_159_32 = new BarMaterial(1, BarShape.FLAT, 0.0159, 0.0032, 0.0159, 50.9, 0.136);
    public static BarMaterial FLAT_191_16 = new BarMaterial(1, BarShape.FLAT, 0.0191, 0.0016, 0.0191, 30.6, 0.082);
    public static BarMaterial FLAT_191_32 = new BarMaterial(1, BarShape.FLAT, 0.0191, 0.0032, 0.0191, 61.1, 0.164);
    public static BarMaterial FLAT_254_16 = new BarMaterial(1, BarShape.FLAT, 0.0254, 0.0016, 0.0254, 40.6, 0.110);
    public static BarMaterial FLAT_254_32 = new BarMaterial(1, BarShape.FLAT, 0.0254, 0.0032, 0.0254, 40.6*2, 0.110*2);
    public static BarMaterial ANGLE_127_32 = new BarMaterial(1.3, BarShape.ANGLE, 0.0127, 0.0032, 0.0127, 71.0, 0.195);
    public static BarMaterial ANGLE_159_16 = new BarMaterial(1.3, BarShape.ANGLE, 0.0159, 0.0016, 0.0159, 48.3, 0.133);
    public static BarMaterial ANGLE_159_32 = new BarMaterial(1.3, BarShape.ANGLE, 0.0159, 0.0032, 0.0159, 91.5, 0.252);
    public static BarMaterial ANGLE_191_16 = new BarMaterial(1.3, BarShape.ANGLE, 0.0191, 0.0016, 0.0191, 58.6, 0.161);
    public static BarMaterial ANGLE_191_32 = new BarMaterial(1.3, BarShape.ANGLE, 0.0191, 0.0032, 0.0191, 112.0, 0.308);
    public static BarMaterial ANGLE_254_16 = new BarMaterial(1.3, BarShape.ANGLE, 0.0254, 0.0016, 0.0254, 78.7, 0.217);
    public static BarMaterial ANGLE_254_32 = new BarMaterial(1.3, BarShape.ANGLE, 0.0254, 0.0032, 0.0254, 152.3, 0.419);
    public static BarMaterial CHANNEL_127_32 = new BarMaterial(1.6, BarShape.CHANNEL, 0.0127, 0.0032, 0.0127, 101.4, 0.279);
    public static BarMaterial CHANNEL_191_32 = new BarMaterial(1.6, BarShape.CHANNEL, 0.0191, 0.0032, 0.0191, 162.9, 0.448);
    public static BarMaterial SQ_TUBE_191_16 = new BarMaterial(1.6, BarShape.SQ_TUBE, 0.0191, 0.0016, 0.0191, 112.0, 0.308);
    public static BarMaterial TSHAPE_127_32 = new BarMaterial(2.6, BarShape.T_SHAPE, 0.0127, 0.0032, 0.0127, 71.0*2, 0.195*2);
    public static BarMaterial TSHAPE_159_16 = new BarMaterial(2.6, BarShape.T_SHAPE, 0.0159, 0.0016, 0.0159, 48.3*2, 0.133*2);
    public static BarMaterial TSHAPE_159_32 = new BarMaterial(2.6, BarShape.T_SHAPE, 0.0159, 0.0032, 0.0159, 91.5*2, 0.252*2);
    public static BarMaterial TSHAPE_191_16 = new BarMaterial(2.6, BarShape.T_SHAPE, 0.0191, 0.0016, 0.0191, 58.6*2, 0.161*2);
    public static BarMaterial TSHAPE_191_32 = new BarMaterial(2.6, BarShape.T_SHAPE, 0.0191, 0.0032, 0.0191, 112.0*2, 0.308*2);
    public static BarMaterial TSHAPE_254_16 = new BarMaterial(2.6, BarShape.T_SHAPE, 0.0254, 0.0016, 0.0254, 78.7*2, 0.217*2);
    public static BarMaterial TSHAPE_254_32 = new BarMaterial(2.6, BarShape.T_SHAPE, 0.0254, 0.0032, 0.0254, 152.3*2, 0.419*2);

    public static String getMaterialName(BarMaterial bm){
        Class<BarMaterials> claz = BarMaterials.class;
        Field[] fields = claz.getDeclaredFields();
        for(Field f:fields){
            f.setAccessible(true);
            try {
                if(bm.equals(f.get(null))){
                    return f.getName();
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public static List<BarMaterial> getTensionBarMaterials(){
        List<BarMaterial> bars = getValues(); //Returns a cloned list so we can safely modify it
        for(BarMaterial b:new ArrayList<BarMaterial>(bars)){ //Iterate over copy to prevent concurrent modification
            if(!b.getBarShape().equals(BarShape.FLAT)){ //Only use flat members for tension
                bars.remove(b);
            }
        }
        return bars;
    }

    public static List<BarMaterial> getCompressionBarMaterials(){
        //TODO Change to only include materials with shapes that we have modelled for buckling
        return getValues(); //Allow all bar materials
    }

    public static List<BarMaterial> getValues(){
        Class<BarMaterials> claz = BarMaterials.class;
        List<BarMaterial> res = new ArrayList<BarMaterial>();
        Field[] fields = claz.getDeclaredFields();
        for(Field f:fields){
            f.setAccessible(true);
            try {
                res.add((BarMaterial) f.get(null));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return res;
    }
}
