package me.eddie.l1applications.materials.bars;

import me.eddie.l1applications.materials.MetalProperties;

/**
 * Created by Edward on 25/11/2017.
 */
public class BarMaterial {
    private BarShape barShape;
    private double size;
    private double thickness;
    private double areaMM;
    private double linearDensity;
    private double barSizeInXYPlane;
    private double beta; //Cost scale factor

    public BarMaterial(double beta, BarShape barShape, double size, double thickness, double barSizeInXYPlane, double areaMM, double linearDensity) {
        this.beta = beta;
        this.barSizeInXYPlane = barSizeInXYPlane;
        this.barShape = barShape;
        this.size = size;
        this.thickness = thickness;
        this.areaMM = areaMM;
        this.linearDensity = linearDensity;
    }

    public double getCostFactorBeta(){
        return this.beta;
    }

    public void setCostFactorBeta(double beta){
        this.beta = beta;
    }

    public double getSizeInXYPlane() {
        return barSizeInXYPlane;
    }

    public void setSizeInXYPlane(double barSizeInXYPlane) {
        this.barSizeInXYPlane = barSizeInXYPlane;
    }

    public double getAreaInMilliMetersSquared(){
        return this.areaMM;
    }

    public long getYoungsModulusGPA(){
        return MetalProperties.ALUMINIUM_YOUNGS_MODULUS_GPA;
    }

    public long getYoungsModulus(){
        return MetalProperties.ALUMINIUM_YOUNGS_MODULUS;
    }

    public BarShape getBarShape() {
        return barShape;
    }

    public void setBarShape(BarShape barShape) {
        this.barShape = barShape;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getThickness() {
        return thickness;
    }

    public void setThickness(double thickness) {
        this.thickness = thickness;
    }

    public double getArea() {
        return this.areaMM/1000000.0;
    }

    public void setAreaMM(double area) {
        this.areaMM = area;
    }

    public double getLinearDensity() {
        return linearDensity;
    }

    public void setLinearDensity(double linearDensity) {
        this.linearDensity = linearDensity;
    }
}
