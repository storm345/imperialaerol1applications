package me.eddie.l1applications.materials.bars;

/**
 * Created by Edward on 25/11/2017.
 */
public enum BarShape {
    FLAT(1),
    ANGLE(1.3),
    CHANNEL(1.6),
    SQ_TUBE(1.7),
    T_SHAPE(2.6);

    private double costFactor;
    private BarShape(double costFactor){
        this.costFactor = costFactor;
    }

    public double getCostFactor(){
        return this.costFactor;
    }
}
