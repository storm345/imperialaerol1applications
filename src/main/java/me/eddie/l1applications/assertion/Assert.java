package me.eddie.l1applications.assertion;

/**
 * Created by Edward on 25/11/2017.
 */
public class Assert {
    public static void assertEquals(double a, double b, double tolerance){
        if(Math.abs(b-a) > tolerance){
            throw new RuntimeException("Assertion failed! "+a+" is not equal to "+b+"!");
        }
    }

    public static void assertEquals(Object a, Object b){
        if(a == null || b == null){
            if(a == b){
                return;
            }
        }
        else if(a.equals(b)){
            return;
        }

        throw new RuntimeException("Assertion failed! "+a+" is not equal to "+b+"!");
    }
}
