package me.eddie.l1applications.main;

import me.eddie.l1applications.materials.bars.BarMaterial;
import me.eddie.l1applications.materials.bars.BarMaterials;
import me.eddie.l1applications.materials.gussets.GussetMaterial;
import me.eddie.l1applications.materials.gussets.GussetMaterials;
import me.eddie.l1applications.materials.gussets.Pin;
import me.eddie.l1applications.materials.gussets.Pins;
import me.eddie.l1applications.pjf.Constraints;
import me.eddie.l1applications.pjf.FailureOutcome;
import me.eddie.l1applications.pjf.FrameworkConfiguration;
import me.eddie.l1applications.pjf.GussetConfiguration;

import javax.xml.ws.Holder;
import java.io.*;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Edward on 25/11/2017.
 */
public class Main {

    public static FrameworkConfiguration getBestConfig(){
        Holder<FrameworkConfiguration> best = new Holder<>(null);
        Holder<Double> bestMerit = new Holder<Double>(0.0d);
        Holder<Integer> combosTried = new Holder<>(0);

        List<GussetMaterial> gussets = GussetMaterials.getValues();
        List<Pin> pinTypes = Pins.getJoiningPins();
        Boolean[] holePosIsLeft = new Boolean[]{true, false};
        ThreadPoolExecutor service = (ThreadPoolExecutor) Executors.newFixedThreadPool(32);
        for(int hole:new int[]{1,2,3,4,9,10}){
            System.out.println("Scheduling hole: "+hole);
            for(int initialDispAboveEMultByHund:new int[]{0}
                    /*int initialDispAboveEMultByHund=1500;initialDispAboveEMultByHund>=0;initialDispAboveEMultByHund-=30*/){
                double initialDispAboveE = initialDispAboveEMultByHund/100000.0;
                BarMaterial[] compressionMats = new BarMaterial[]{/*BarMaterials.TSHAPE_254_32,*/BarMaterials.TSHAPE_254_16};
                for(BarMaterial compressionMat:compressionMats){
                    BarMaterial[] t12Mats = new BarMaterial[]{BarMaterials.FLAT_191_32,BarMaterials.FLAT_254_32};
                    for(BarMaterial t12Mat:t12Mats){
                        int maxPins = 5;
                        int minPins = 1;
                        for(int numPins12Bottom=minPins;numPins12Bottom<=maxPins;numPins12Bottom++){
                            for(int numPins12Left:new int[]{numPins12Bottom}){
                                for(int numPins13Left=minPins;numPins13Left<=maxPins;numPins13Left++){
                                    for(int numPins13Right:new int[]{numPins13Left}){
                                        for(int numPins23Right=minPins;numPins23Right<=maxPins;numPins23Right++){
                                            for(int numPins23Bottom:new int[]{numPins23Right}){
                                                for(GussetMaterial gussetMaterialLeft:new GussetMaterial[]{GussetMaterials.SHEET_1_5MM}){
                                                    for(GussetMaterial gussetMaterialBottom:new GussetMaterial[]{GussetMaterials.SHEET_1_5MM}){
                                                        for(GussetMaterial gussetMaterialRight:gussets){
                                                            for(Pin pinType12Bottom:pinTypes){
                                                                for(Pin pinType12Left:new Pin[]{pinType12Bottom}){
                                                                    for(Pin pinType13Left:pinTypes){
                                                                        for(Pin pinType13Right:new Pin[]{pinType13Left}){
                                                                            for(Pin pinType23Right:pinTypes){
                                                                                for(Pin pinType23Bottom:new Pin[]{pinType23Right}){
                                                                                    for(boolean holePosLeftLeft:new Boolean[]{true}){
                                                                                        for(boolean holePosLeftBottom:new Boolean[]{true}){
                                                                                            for(boolean holePosLeftRight:new Boolean[]{true}){
                                                                                                //long t1 = System.nanoTime();
                                                                                                GussetConfiguration leftGusset = new GussetConfiguration(gussetMaterialLeft,
                                                                                                        numPins13Left, numPins12Left, pinType13Left, pinType12Left, holePosLeftLeft, true);
                                                                                                GussetConfiguration rightGusset = new GussetConfiguration(gussetMaterialRight,
                                                                                                        numPins23Right, numPins13Right, pinType23Right, pinType13Right, holePosLeftRight, true);
                                                                                                GussetConfiguration bottomGusset = new GussetConfiguration(gussetMaterialBottom,
                                                                                                        numPins12Bottom, numPins23Bottom, pinType12Bottom, pinType23Bottom, holePosLeftBottom, true);
                                                                                                final FrameworkConfiguration config1 = new FrameworkConfiguration(hole,
                                                                                                        12000,initialDispAboveE,initialDispAboveE,
                                                                                                        t12Mat,
                                                                                                        compressionMat,
                                                                                                        BarMaterials.FLAT_191_32,
                                                                                                        leftGusset,
                                                                                                        bottomGusset,
                                                                                                        rightGusset
                                                                                                );
                                                                                                combosTried.value++;
                                                                                                service.submit(new Runnable() {
                                                                                                    @Override
                                                                                                    public void run() {
                                                                                                        FailureOutcome fo = config1.willFail();
                                                                                                        if(!fo.isFailed()) {
                                                                                                            double merit = config1.getMeritFunction();
                                                                                                            if (merit > bestMerit.value) {
                                                                                                                synchronized (best) {
                                                                                                                    if (merit > bestMerit.value) {
                                                                                                                        bestMerit.value = merit;
                                                                                                                        best.value = config1;
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                });
                                                                                                //long t2 = System.nanoTime();
                                                                                                //actualConsole.println(t2-t1);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        actualConsole.println("All tasks posted ("+combosTried.value+")");
        BlockingQueue<Runnable> q = service.getQueue();
        while(q.size() > 0){
            try {
                System.out.println("Remaining combos: "+q.size());
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        service.shutdown();

        return best.value;
    }

    public static PrintStream actualConsole;

    public static void main(String[] args){
        //TODO

        File file = new File("output.txt");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        try {
            PrintStream consoleOut = System.out;
            actualConsole = consoleOut;
            PrintStream fileOut = new PrintStream(
                    new BufferedOutputStream(new FileOutputStream(file)));
            System.setOut(new FileAndConsoleStream(consoleOut, fileOut));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        GussetConfiguration gussetLeft = new GussetConfiguration(GussetMaterials.SHEET_1_5MM,
                2, 3, Pins.BOLT_6, Pins.BOLT_6, true, true);
        GussetConfiguration gussetBottom = new GussetConfiguration(GussetMaterials.SHEET_1_5MM,
                3, 2, Pins.BOLT_6, Pins.BOLT_6, false, true);
        GussetConfiguration gussetRight = new GussetConfiguration(GussetMaterials.SHEET_1_5MM,
                2, 2, Pins.BOLT_6, Pins.BOLT_6, true, true);

        FrameworkConfiguration config1 = new FrameworkConfiguration(3, 12000, 0/1000.0d,
                0, BarMaterials.FLAT_254_32, BarMaterials.TSHAPE_254_16, BarMaterials.FLAT_191_32,
                gussetLeft,
                gussetBottom,
                gussetRight);
        FailureOutcome fo = config1.willFail();
        System.out.println("Fails: "+fo.isFailed());
        if(fo.isFailed()) {
            System.out.println("Cause: " + fo.getCause());
        }

        /**/

        /*System.out.println("Calculating best config...");

        FrameworkConfiguration config1 = getBestConfig();
        if(config1 == null){
            System.out.println("Every design failed");
            return;
        }*/

        printOut(config1);

        System.out.println("Complete!");
        System.out.flush();

        /*FrameworkConfiguration other = new FrameworkConfiguration(3, 12000, 0, 0,
                BarMaterials.FLAT_254_32, BarMaterials.TSHAPE_254_16, BarMaterials.FLAT_191_32);
        FailureOutcome failureOutcome = other.willFail();
        if(failureOutcome.isFailed()){
            System.out.println("OTHER WILL FAIL: "+failureOutcome.getCause());
        }
        printOut(other);
        config1.validateDisplacementsWork();*/
    }

    public static void printOut(FrameworkConfiguration config1){
        System.out.println("Global tolerance: "+Constraints.PREFFERED_SAFETY_TOLERANCE*100+"% of theoretical maximum");
        System.out.println("Tension member tolerance: "+Constraints.BAR12_NET_TENSION_SAFETY_TOLERANCE*100+"% of theoretical maximum");
        System.out.println("Hole: "+config1.getLeftHoleNumber());
        System.out.println("Initial disp above E: "+config1.getBaseConnectionInitialDispAboveE()*1000+"mm");
        System.out.println("Bar 12 material: "+BarMaterials.getMaterialName(config1.getBar12().getMaterial()));
        System.out.println("Bar 13 material: "+BarMaterials.getMaterialName(config1.getBar13().getMaterial()));
        System.out.println("Bar 23 material: "+BarMaterials.getMaterialName(config1.getBar23().getMaterial()));
        System.out.println("Gusset and pin info:");
        System.out.println("Left gusset: ");
        config1.getGussetLeft().printOut();
        System.out.println("Member 13 left pin config:");
        config1.getPinConfiguration13Left().printOut();
        System.out.println("Member 12 left pin config:");
        config1.getPinConfiguration12Left().printOut();
        System.out.println("Right gusset: ");
        config1.getGussetRight().printOut();
        System.out.println("Member 13 right pin config:");
        config1.getPinConfiguration13Right().printOut();
        System.out.println("Member 23 right pin config:");
        config1.getPinConfiguration23Right().printOut();
        System.out.println("Bottom gusset: ");
        config1.getGussetBottom().printOut();
        System.out.println("Member 12 bottom pin config:");
        config1.getPinConfiguration12Bottom().printOut();
        System.out.println("Member 23 bottom pin config:");
        config1.getPinConfiguration23Bottom().printOut();
        System.out.println("");
        System.out.println("");
        /*FrameworkConfiguration config1 = new FrameworkConfiguration(1,
            12000,0.15,0,
                BarMaterials.FLAT_191_32,
                BarMaterials.TSHAPE_254_32,
                BarMaterials.FLAT_191_32
        );*/
        System.out.println("Left support coords: ("+config1.getLeftSupportInitialPos().getX()+"m, "+ config1.getLeftSupportInitialPos().getY()+"m)");
        System.out.println("Bottom support coords: ("+config1.getBottomSupportInitialPos().getX()+"m, "+ config1.getBottomSupportInitialPos().getY()+"m)");
        System.out.println("Right support coords: ("+config1.getRightSupportInitialPos().getX()+"m, "+ config1.getRightSupportInitialPos().getY()+"m)");
        System.out.println("Bar12 Length (Hole center to hole center): "+(config1.getBar12().getLength()*1000)+"mm");
        System.out.println("Bar13 Length (Hole center to hole center): "+(config1.getBar13().getLength()*1000)+"mm");
        System.out.println("Bar23 Length (Hole center to hole center): "+(config1.getBar23().getLength()*1000)+"mm");
        double v1 = config1.getLeftSupportReaction().getY();
        double v2 = config1.getRightSupportReaction().getY();
        System.out.println("Left support rection force: "+v1+"N");
        System.out.println("Right support reacton force: "+v2+"N");
        double t12 = config1.getTension12Mag();
        double t13 = config1.getTension13Mag();
        double t23 = config1.getTension23Mag();
        System.out.println("Tension in bar12: "+t12+"N");
        System.out.println("Tension in bar13: "+t13+"N");
        System.out.println("Tension in bar23: "+t23+"N");
        System.out.println("Extension in bar 12: "+(config1.getExt12()*1000)+"mm");
        System.out.println("Extension in bar 13: "+(config1.getExt13()*1000)+"mm");
        System.out.println("Extension in bar 23: "+(config1.getExt23()*1000)+"mm");
        System.out.println("Vertical Displacement at loading point: "+(config1.getBottomSupportDisplacement().getY()*1000)+"mm");
        System.out.println("Horizontal Displacement at loading point: "+(config1.getBottomSupportDisplacement().getX()*1000)+"mm");
        System.out.println("Horizontal Displacement at right support: "+(config1.getRightSupportDisplacement().getX()*1000)+"mm");
        System.out.println("Undeformed clearance from obstacle: "+(config1.getUndeformedClearanceFromObstacle()*1000)+"mm");
        System.out.println("Deformed clearance from obstacle: "+(config1.getDeformedClearanceFromObstacle()*1000)+"mm");
        System.out.println("Stress in bar 13: "+config1.getStressMPa13()+"MPa");
        System.out.println("Stress in bar 23: "+config1.getStressMPa23()+"MPa");
        System.out.println("Stress in bar 12: "+config1.getStressMPa12()+"MPa");
        System.out.println("L/b for bar 13: "+(config1.getBar13().getLength()/config1.getBar13().getMaterial().getSize()));
        System.out.println("Buckles in top member: "+config1.willBar13Buckle());
        System.out.println("Distance from buckling: "+config1.getBar13LoadDistanceFromBuckling()+"kN after including safety tolerance of "+ Constraints.SAFETY_TOLERANCE_BUCKLING*100+"%");
        System.out.println("Alpha (Angle between compressive member and horz. from right support with +ve upwards): "+config1.getInitialAlphaRadians()+" rad");
        System.out.println("Beta (Angle between bottom-right member and horz. from right support with +ve downwards): "+config1.getInitialBetaRadians()+" rad");
        System.out.println("Gamma (Angle between left member and horz. from loading hole with +ve upwards): "+config1.getInitialGammaRadians()+" rad");

        System.out.println("");
        System.out.println("Predicted characteristics: (Not exact)");
        System.out.println("Mass: "+config1.getTotalMassGrams()+"g");
        System.out.println("Cost: "+config1.getTotalCost());
        System.out.println("Merit: "+config1.getMeritFunction());
    }
}
