package me.eddie.l1applications.main;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by Edward on 10/12/2017.
 */
public class FileAndConsoleStream extends PrintStream {
    private final PrintStream second;
    public FileAndConsoleStream(OutputStream main, PrintStream second) {
        super(main);
        this.second = second;
    }

    /*@Override
    public void println(String msg){
        second.println(msg);
        second.flush();
        main.println(msg);
    }*/

    /**
     * Closes the main stream.
     * The second stream is just flushed but <b>not</b> closed.
     * @see java.io.PrintStream#close()
     */
    @Override
    public void close() {
        // just for documentation
        second.flush();
        second.close();
        super.close();
    }

    @Override
    public void flush() {
        super.flush();
        second.flush();
    }

    @Override
    public void write(byte[] buf, int off, int len) {
        super.write(buf, off, len);
        second.write(buf, off, len);
    }

    @Override
    public void write(int b) {
        super.write(b);
        second.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        super.write(b);
        second.write(b);
    }
}
